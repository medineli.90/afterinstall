# afterInstall
# update system : 
sudo apt update && sudo apt upgrade -y

# Install python and zsh shell
sudo apt install -y python3 python3-pip zsh ffmpeg

# Change shell 
chsh
/bin/zsh

# Install ohmyzsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

Install theme powerlevel10

git clone https://github.com/romkatv/powerlevel10k.git /home/medineli/.oh-my-zsh/themes/powerlevel10k
nano ~/.zshrc
ZSH_THEME="powerlevel10k/powerlevel10k"


# Install ohmyzsh plugins
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions\
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting\

# Add plugins to ~/.zshrc file 
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)

# get yt-dlp + chmod +x yt-dlp
wget https://github.com/yt-dlp/yt-dlp/releases/download/2021.10.10/yt-dlp

# Copy alias to .zshrc
# youtube-dl
alias dlmp3='/bin/yt-dlp --no-check-certificate -f "bestaudio/best[height<=480]" --extract-audio --audio-format mp3 -i -o "%(title)s.%(ext)s"'
#one video
alias dloneVideoHigh='/bin/yt-dlp  --no-check-certificate  -f bestvideo​+bestaudio/best -i -o "%(title)s.%(ext)s"'
alias dloneVideoHighMp4='/bin/yt-dlp  --no-check-certificate  -f bestvideo​[ext!=webm]‌​+bestaudio/best -i -o "%(title)s.%(ext)s"'
#playList
alias dlsorted-pl='/bin/yt-dlp  --no-check-certificate  -f bestvideo+bestaudio -i -o "%(playlist_index)s-%(title)s.%(ext)s"'
alias dlsortedBestVideoSound-pl='/bin/yt-dlp  --no-check-certificate  -f bestvideo+bestaudio/best -i -o "%(playlist_index)s-%(title)s.%(ext)s"'
alias dlsortedHighMp4-pl='/bin/yt-dlp  --no-check-certificate  -f bestvideo[ext!=webm]‌​+bestaudio/best -i -o "%(playlist_index)s-%(title)s.%(ext)s"'
alias dlhigh-pl='/bin/yt-dlp  --no-check-certificate  -f bestvideo​+bestaudio/best -i -o "%(title)s.%(ext)s"'
alias dlhighMp4-pl='/bin/yt-dlp  --no-check-certificate  -f bestvideo[ext!=webm]‌​+bestaudio/best -i -o "%(title)s.%(ext)s"'

# Install udemdl
# clone
git clone https://github.com/r0oth3x49/udemy-dl.git
# Install requirements 
pip3 install six m3u8 colorama requests unidecode pyOpenSSL cloudscraper 


#I add my config after install linux dest.


#software:
sudo apt update && sudo apt install git mint-meta-codecs vlc audacious docky ffmpeg tlp tlp-rdw keepassxc uget gimp gedit python3-pip



#pardus: 
sudo apt update && sudo apt install git ffmpeg tlp tlp-rdw keepassxc uget gimp gedit 

#add git to windows terminla
#https://stackoverflow.com/questions/56839307/adding-git-bash-to-the-new-windows-terminal

#battery: 
    sudo tlp start

#youtube-dl:
    #01
    pip3 install youtube-dl -U

    #2
    sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
    sudo chmod a+rx /usr/local/bin/youtube-dl




#Flutter Conf.
#After dowload flutter  :
1- extrac it : tar xf flutter.tar
2- move it where ever want and paste :  export PATH="$PATH:`pwd`/flutter/bin"  

1- extrac it to flutter but it in home 
2- open /etc/environment as root 
and add line below like 
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/home/medineli/IDE/flutter/bin:/usr/local/android-studio/bin"
FLUTTER="/home/medineli/IDE/flutter/bin"
ANDROID_HOME="/usr/local/android-studio/bin"

dont foget 
1- flutter config --android-sdk /home/medineli/Android/sdk
2- flutter doctor --android-licenses

* install sdk-toolfrom android-studio : Android-SDK> uncheck "Hide OBsolete Pack" , check "Android SDK Tools" &&&&& reboot system






#aliass

#SysTem
alias up='sudo apt update && sudo apt upgrade -y'
alias clean='sudo apt-get autoclean && sudo apt-get clean && sudo apt-get autoremove'
alias install='sudo apt install'
alias remove='sudo apt remove'	


# pythin conda alias
alias sconda='conda config --show | grep auto_activate_base'
alias aconda='conda config --set auto_activate_base True && source ~/.bashrc'
alias dconda='conda config --set auto_activate_base False && source ~/.bashrc'
alias jrun='jupyter-notebook'


# GeeK AliAs
alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias .....='cd ../../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../..'


#git 
alias gpush='git add . && git commit -m "first Commit" && git push origin master'

#Network Things
alias ping='ping -c 5'
alias showip='ifconfig'

#Network Things
alias ping='ping -c 5'
alias showip='ifconfig'

# youtube-dl
#audio
alias dlmp3='youtube-dl --no-check-certificate -f "bestaudio/best[height<=480]" --extract-audio --audio-format mp3 -i -o "%(title)s.%(ext)s"'
#one video
alias dloneVideoHigh='youtube-dl --no-check-certificate  -f bestvideo​+bestaudio/best -i -o "%(title)s.%(ext)s"'
alias dloneVideoHighMp4='youtube-dl --no-check-certificate  -f bestvideo​[ext!=webm]‌​+bestaudio/best -i -o "%(title)s.%(ext)s"'
#playList
alias dlsorted-pl='youtube-dl --no-check-certificate  -f bestvideo+bestaudio -i -o "%(playlist_index)s-%(title)s.%(ext)s"'
alias dlsortedBestVideoSound-pl='youtube-dl --no-check-certificate  -f bestvideo+bestaudio/best -i -o "%(playlist_index)s-%(title)s.%(ext)s"'
alias dlsortedHighMp4-pl='youtube-dl --no-check-certificate  -f bestvideo[ext!=webm]‌​+bestaudio/best -i -o "%(playlist_index)s-%(title)s.%(ext)s"'
alias dlhigh-pl='youtube-dl --no-check-certificate  -f bestvideo​+bestaudio/best -i -o "%(title)s.%(ext)s"'
alias dlhighMp4-pl='youtube-dl --no-check-certificate  -f bestvideo[ext!=webm]‌​+bestaudio/best -i -o "%(title)s.%(ext)s"'





1- sudo apt update && sudo apt upgrade -y
2- sudo apt install zsh ffmpeg python3 python3-pip
-switch to zsh: chsh -> /bin/zsh reload terminal
3- install ohh my zsh : sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
4- Install two plugin of oh-my-zsh
zsh plugin
theme git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

- git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions\
- git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting\
add plugin name to zshrc : 

plugins=(git zsh-autosuggestions zsh-syntax-highlighting)



5- copy to /bin/ && chmod : yt-dlp 
add alias to zshrc from gitlab
source ~/.zshrc

6- git clone https://github.com/r0oth3x49/udemy-dl.git
 pip3 install -r requirements.txt
Test it : python3 ./ud/udemy-dl/udemy-dl.py  https://www.udemy.com/course/1-saatte-css-ogren/ -q 240 -o "/mnt/d/video/programing/django-rest"

